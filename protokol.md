# Protokol

- Téma: manga
- Vypracovali: Reháh Jan, Vlček Adam, Pták Ondřej
- Třída: 3.H

## Zdroj dat

Kaggle - 🎉💥Check-out My New Dataset "best-selling-manga"🔥🔥🔥. Online. 2023. Dostupné z: <https://www.kaggle.com/discussions/general/424769>. [cit. 2024-05-16].

Kaggle - Manga Dataset. Online. 2021. Dostupné z: <https://www.kaggle.com/datasets/darknez/manga-dataset/data?select=May3_2021.csv>. [cit. 2024-05-16].

Kaggle - MyAnimeList Anime and Manga Datasets. Online. 2023. Dostupné z: <https://www.kaggle.com/datasets/andreuvallhernndez/myanimelist>. [cit. 2024-05-16].

## Zpracování dat

## Role členů týmu

## Náhledy dashboardu
