# Urls

best-selling-manga.csv - <https://www.kaggle.com/discussions/general/424769>

Manga Dataset (May3_2021.csv) -
<https://www.kaggle.com/datasets/darknez/manga-dataset/data?select=May3_2021.csv>

MAL manga - <https://www.kaggle.com/datasets/andreuvallhernndez/myanimelist>

top 1000 - <https://www.kaggle.com/datasets/astronautelvis/top-1000-ranked-mangas-by-myanimelist>

manga - <https://www.kaggle.com/datasets/itokianarafidinarivo/myanimelist-mangas-2020>
