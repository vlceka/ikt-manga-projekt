IKT Vytvořte si v rámci skupiny tým skládající se z 2 - 3 členů.
Na základě dostupnosti dat si vymyslete téma (pokud možno obecné), které budete zpracovávat.
Vytvořte si projekt na školním GitLabu (gitlab.spseplze.cz), kam si budete průběžně ukládat svoji práci včetně dat a protokolu.
Získejte data alespoň ze tří zdrojů a vytvořte alespoň pět analytických otázek/úkolů - orientační termín -> konec září/února.
Upravte/vyčistěte data (použijte DAX), vytvořte multidimenzionální model - orientační termín -> polovina listopadu/dubna
Vytvořte vhodné vizualizace pro otázky/úkoly z bodu 4 - orientační termín -> konec prosince/května. Vizualizace budou mít správné popisky (hlavně použijte jeden jazyk).
Připravte si prezentaci/výstup a předveďte ji na hodině IKT – začátek ledna/června
Do MS Teams odevzdáte protokol o průběhu práce (odkud máte data, zpracování dat, úkoly jednotlivých členů týmu) a náhledy všech dashboardů.
Do komentáře k tomuto příspěvku napíše jeden člen týmu jeho složení a vybrané téma. V případě shody tématu preferuji rychlejší tým, pomalejší si musí vybrat jiné téma.