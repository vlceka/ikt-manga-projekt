# Analytické otázky

1. Průměrný počet prodaných kopií podle demografie
2. Zastoupení žánrů podle demografie
3. Množství mang podle vydavatele
4. Poměr dokončených, vycházejících, zrušených a pozastavených mang
5. Popularita žánru
